SELECT * FROM studentsperformance

-- 1. Berapa jumlah record pada dataset tersebut?
SELECT count(*) AS jumlah_record 
FROM studentsperformance;

-- 2. Buatlah query untuk menunjukkan:
-- 2.a perbandingan rata-rata nilai reading laki-laki dan perempuan
SELECT gender, AVG(reading_score) AS reading_score_rata_rata 
FROM studentsperformance
GROUP BY gender;
-- 2.b nilai math tertinggi dan terendah dari masing-masing tingkatan pendidikan orangtua (parental level of education)
SELECT parental_level_of_education, 
MAX(math_score), MIN(math_score) FROM studentsperformance
GROUP BY parental_level_of_education
ORDER BY parental_level_of_education;

-- 3. Berapa nilai rata-rata dari math, reading dan writing orang bergender perempuan yang pernah / sudah menyelesaikan kursus persiapan ujian (test_preparation_course)?
SELECT gender, test_preparation_course,
AVG(math_score), AVG(reading_score), AVG(writing_score) 
FROM studentsperformance
WHERE gender = 'female' AND test_preparation_course = 'completed';

-- 4. Berapa nilai rata-rata writing orang yang memiliki orangtua yang tingkat pendidikannya adalah high school / some high school? (digabung, tidak terpisah)
SELECT AVG(writing_score) AS avg_writing_score
FROM studentsperformance
WHERE parental_level_of_education = 'high school' 
OR parental_level_of_education = 'some high school';

-- 5 LENGKAPI QUERY
SELECT	gender,
race_or_ethnicity, test_preparation_course, AVG(math_score) AS avg_math_score,
AVG(reading_score) AS avg_reading_score,
AVG(writing_score) AS avg_writing_score
FROM studentsperformance GROUP BY gender,
race_or_ethnicity, test_preparation_course
ORDER BY gender,
race_or_ethnicity,
test_preparation_course;

-- 6
SELECT	gender,
race_or_ethnicity, test_preparation_course, AVG(math_score) AS avg_math_score,
AVG(reading_score) AS avg_reading_score,
AVG(writing_score) AS avg_writing_score
FROM studentsperformance GROUP BY gender,
race_or_ethnicity, test_preparation_course
HAVING AVG(math_score) > 70
ORDER BY gender,
race_or_ethnicity,
test_preparation_course;


